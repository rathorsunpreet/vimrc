" -------------------------------------------
" .vimrc compiled by Sunpreet Singh Rathor 
" Location: https://bitbucket.org/rathorsunpreet/vimrc/src/main/vimrc
"--------------------------------------------
" Note: You would get an error when running this vimrc
" for the first time with unknown function
" SyntasticStatuslineFlag which is due to
" Syntastic not being loaded when vim reaches
" that particular setting, skip it for now
" Uncomment lines as needed


" Don't try to be VI Compatible
set nocompatible

" -------------------------------------------
" Encoding Settings
"--------------------------------------------
" Set encoding
" Also needed for NerdTree Icons
set encoding=utf-8

" Disable modifyOtherKeys feature (stops printing weird characters)
set t_TI= t_TE=

" Set filetype to off, helps load plugins properly
filetype off


" -------------------------------------------
" Plugin Setup
"--------------------------------------------
" Auto Install Plug if not installed
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Auto Install Plugins if missing
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)')) | PlugInstall --sync | source $MYVIMRC | endif

call plug#begin()
" Add File Explorer
Plug 'preservim/nerdtree'

" Add syntax checking
Plug 'vim-syntastic/syntastic'

" Add Status Line
Plug 'itchyny/lightline.vim'

" Enclosing characters made easy
Plug 'tpope/vim-surround'

" Add HTML/CSS shortcut snippets + Lorem Ipsum Generator
Plug 'mattn/emmet-vim' 

" Display list of open buffers on tabline
Plug 'ap/vim-buftabline'

" Add sidebar to display git diff in side column
Plug 'airblade/vim-gitgutter'

" Allows git commands from within vim
Plug 'tpope/vim-fugitive'

" Add Git Icons to NerdTree
Plug 'Xuyuanp/nerdtree-git-plugin'

" Add Nerdtree File type Icons
" Note: Install Nerd Fonts in local folder
" ($HOME/.local/share/fonts) and then run fc-cache -f -v in terminal
" Then set terminal to use installed font.
" Restart terminal to activate new font.
Plug 'ryanoasis/vim-devicons'

" Add interface style similar to Turbo C++
"Plug 'skywind3000/vim-quickui'

" Add syntax highlighting and indentation for multiple languages
Plug 'sheerun/vim-polyglot'

call plug#end()


" -------------------------------------------
" Plugin Settings
"--------------------------------------------
" Close NERDTree if it's the only one open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Toggle NERDTree with keystroke <F6>
nmap <F6> :NERDTreeToggle<CR>

" Set current working directory to open file location
let g:NERDTreeChDirMode=2

" Default Settings for Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
" Set Quickfix foreground color to black
highlight QuickFixLine ctermfg=none ctermbg=none cterm=none

" Lightline Settings
" Comment below line if statusline is alright
set laststatus=2
" Stop vim from showing own status
set noshowmode
" Add color to statusline
if !has('gui_running')
    set t_Co=256
endif

" Emmet Settings
" Enable Emmet on HTML and CSS files only
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall
" Redefine trigger key
let g:user_emmet_leader_key=','| " Trigger key is now ,,

" Buftabline Settings
set hidden
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>

" Set Font for NerdTree Icons
" Uncomment below line if using GUI
" Note: format is set guifont=<FONT_NAME> <FONT_SIZE>
" For Linux
" set guifont=DroidSansMono\ Nerd\ Font\ 11
" For Mac/Windows
" set guifont=DroidSansMono\ Nerd\ Font:h11
" or
" set guifont=DroidSansMono_Nerd_Font:h11

" -------------------------------------------
" Miscellaneous Settings
"--------------------------------------------
" Security - do not read settings from other files
set modelines=0

" Show line numbers
set number

" Enable line wrapping
set wrap

" Wrap at break points and not in the middle of a word
set linebreak

" Allow mouse scrolling
set mouse=a

" Show matching brackets
set showmatch

" Set currently opened file's directory as root
set autochdir

" -------------------------------------------
" Folding Settings
"--------------------------------------------
set foldmethod=indent| " Set fold based on indent
set foldcolumn=2| " Show fold column on the left


" -------------------------------------------
" Indentation Settings
"--------------------------------------------
" New indentation inherits from previous lines
set autoindent

" Allow backspace over indents, insertion starts and line breaks
set backspace=indent,eol,start


" -------------------------------------------
" Search Settings
"--------------------------------------------
set hlsearch| " Highlight search
set incsearch| " Incremental search
set ignorecase| " Ignore case while searching
set smartcase| " Switch search when query has uppercase character


" -------------------------------------------
" Dsiable Backup Files
"--------------------------------------------
set nobackup| " Stop vim from creating backup files
set nowritebackup| " Don't create backup file while editing
set noswapfile| " Don't create swap files


" -------------------------------------------
" Auto Bracket/Quotes Settings
"--------------------------------------------
" For ()
:inoremap ( ()<Esc>i
" For []
:inoremap [ []<Esc>i
" For {}
:inoremap { {}<Esc>i
" For <>
:inoremap < <><Esc>i
" For ""
:inoremap " ""<Esc>i
" For ''
:inoremap ' ''<Esc>i


" -------------------------------------------
" Shortcut for Cut / Copy / Paste to / from
" System Clipboard
"--------------------------------------------
" Uncomment the appropriate line as per system / vim setup
" Check if vim was compiled with clipboard support
if has('clipboard')
    " Add keystroke F7 to cut into system clipboard
    " vnoremap <F7> "+x
    " Add keystroke F7 to copy into system clipboard
    "vnoremap <F7> "+y
    " Add keystroke F8 to paste from system clipboard
    "nnoremap <F8> "+gP
" Check if xclip is installed
elseif !empty(glob('/usr/bin/xclip')) && !isdirectory('/usr/bin/xclip')
    " Add keystroke F7 to copy into xclip
    vnoremap <F7> :'<,'>w !xclip -selection clipboard<CR><CR>
    " Add keystroke F8 to paste from xclip
    nnoremap <F8> :r !xclip -o -selection clipboard<CR>
" Check if xsel is installed
elseif !empty(glob('/usr/bin/xsel'))  && !isdirectory('/usr/bin/xsel')
    " Add keystroke F7 to copy into xsel
    "vnoremap <F7> :'>,'> !tee >(xsel --clipboard)<CR>
    " Add keystroke F8 to paste from xsel
    "nnoremap <F8> :r !xsel --clipboard<CR>
endif